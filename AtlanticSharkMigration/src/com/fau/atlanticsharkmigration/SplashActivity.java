package com.fau.atlanticsharkmigration;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		Thread timer= new Thread() {
			public void run(){
				try{
					sleep(5000);// set delay on the splash screen for 5000 ms
										
				} catch (InterruptedException e){
					e.printStackTrace();
					
				}finally{
					Intent openMain= new Intent("com.fau.atlanticsharkmigration.MainActivity");
					startActivity(openMain);
				}
			}
			
		};
		timer.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash, menu);
		return true;
	}

}
