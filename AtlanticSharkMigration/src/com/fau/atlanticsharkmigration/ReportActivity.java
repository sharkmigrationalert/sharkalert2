package com.fau.atlanticsharkmigration;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class ReportActivity extends Activity { 
	
	
	//button reference
	Button submit;
	//Check box references
	CheckBox  analmarking,pectoralfin,behindpectorialfin;
	
	//GPS references 
	TextView textlat;  //for latitude
	TextView textlong;  //for longitude
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_report);
		
		
//////////////////////////date///////////////////////////////////////////////////		
		//setting the current date  and displaying  it in the  text view tvcurrentdate
		TextView date = (TextView) findViewById( R.id.tvcurrentdate);
		SimpleDateFormat sdf = new SimpleDateFormat( "MM/dd/yy" ); 
		date.setText( sdf.format( new Date() ));

		
/////////////////////////////////////////////////////////////////////////////////
		
		
		
		//GPS setting references to objects
		textlat = (TextView)findViewById(R.id.tvlatitude);
		textlong = (TextView)findViewById(R.id.tvlong);
		
		LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		LocationListener ll= new mylocationlistener();
		
		//requesting location updates in 1000ms every 10meters
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, ll);

////////////////////////////////////////////////////////////////////////////////////		
		
		//setting references for check boxes 
		analmarking = (CheckBox)findViewById(R.id.checkBoxanalmarking);
		
		pectoralfin = (CheckBox)findViewById(R.id.checkpectorialfin);
		
		behindpectorialfin = (CheckBox)findViewById(R.id.checkBoxbehindpectorial);
		
		
		
		
		
		
		//setting reference to the button submit
		submit = (Button)findViewById(R.id.bsubmit);
		
		//stting up onclick listener for button
		submit.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				//when the submit button is press start transmitting to data base
				// TODO Auto-generated method stub
				
				
				//conditional for the anal marking check box 
				if(analmarking.isChecked())
				{
					//sent to database code goes here!!!!
					
					//this is a message that displays if the analmarking check box is checked and the submit button is pressed
					Toast.makeText(getBaseContext(), "has anal marking", Toast.LENGTH_SHORT).show();
					
					
				}
				
				//conditional for the pectoral fin check box 
				if(pectoralfin.isChecked())
				{
					//sent to database code goes here!!!
					
					//this is a message that displays if the pectorla check box is checked and the submit button is pressed
					Toast.makeText(getBaseContext(), "has pectoral marking", Toast.LENGTH_SHORT).show();
					
					
				}
				
				//conditional for the behind pectoral fin check box 
				if(behindpectorialfin.isChecked())
				{
					//sent to database code goes here
					//this is a message that displays if the behind pectoral check box is checked and the submit button is pressed
					Toast.makeText(getBaseContext(), "has behind pectoral", Toast.LENGTH_SHORT).show();
					
					
				}
				

			}//end of onClick
		});//end of setOnclicklistener
		
		
		
		
		
		
/////////////////////////////////////////////////////////////////////////////		
		
		//do not erase this bracket this is for the main bracket
	}
	
	
/////////////////////////////////////////////////////////////////////////////////	
		//internal class for GPS
		 class mylocationlistener implements LocationListener
		 {

			@Override
			public void onLocationChanged(Location location)
			{
				// TODO Auto-generated method stub
				if(location !=null)
				{
					//getting latitude and longitude, this two variables hold the actual longitude and latidue
					double pLong= location.getLongitude();
					double pLat = location.getLatitude();
					//now we are going to display the latitude and longitude in the boxes
					textlat.setText(Double.toString(pLat));
					textlong.setText(Double.toString(pLong));
				}
				
			}

			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				// TODO Auto-generated method stub
				
			}
			
		}
///////////////////////////////////////////////////////////////////////////////////////////////
		 

		 
		 
		
	
/////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.report, menu);
		return true;
	}

}
