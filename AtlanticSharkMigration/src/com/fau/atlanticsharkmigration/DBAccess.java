/*
Example InsertData:

	List<NameValuePair> parameterList = new ArrayList<NameValuePair>();
	parameterList.add(new BasicNameValuePair("DorsalFinHeight", "23"));
	parameterList.add(new BasicNameValuePair("ForkedTailLength", "34"));
	parameterList.add(new BasicNameValuePair("AnalFinMarking", "NO"));
	parameterList.add(new BasicNameValuePair("PectoralFinMarking", "NO"));
	parameterList.add(new BasicNameValuePair("KCMTagNumber", "1234533"));
	parameterList.add(new BasicNameValuePair("Latitude", "910"));
	parameterList.add(new BasicNameValuePair("Longitude", "710"));
	parameterList.add(new BasicNameValuePair("AnglerName", "Juca Pirama"));
	parameterList.add(new BasicNameValuePair("BoatName", "The Pirama"));
	            	
	DBAccess dba = new DBAccess();
	String ResponseMessage = dba.insertData(parameterList);


Example GetSharkPosition()
	
	DBAccess dba = new DBAccess();
	                
	List<DBAccess.sharkPosition> sharkPositionList = dba.GetSharkLocation();
	String returnMessage = new String();
	                
	for(int i=0; i < sharkPositionList.size(); i++)
	{
		DBAccess.sharkPosition sp = sharkPositionList.get(i);
	    returnMessage += sp.latitude + ":" + sp.longitude + ":" + sp.sharkType + System.getProperty("line.separator");
	}

*/

package com.fau.atlanticsharkmigration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.StrictMode;

public class DBAccess{

	public String insertData(List<NameValuePair> parameterList)
	{
		
		String parameters = "";
		
		// iterates through the list of parameters and adds each one to the parameter string
    	for(int i=0; i < parameterList.size(); i++)
    	{
    		try {
    			// Adds the ampersand between the parameters  
    			if (parameters.length() > 0)
    				parameters += "&";
    			else
    				parameters += "?";
    		
    			// Adds each parameter to the list 
    			NameValuePair parameter = parameterList.get(i);
    		
				parameters += URLEncoder.encode(parameter.getName(), "UTF-8") + "=" + URLEncoder.encode(parameter.getValue(), "UTF-8");
				
			} catch (UnsupportedEncodingException e) {
				
				// TODO Auto-generated catch block
				parameters +="juca=crazy"; // Just a placeholder for the failed parameter
				e.printStackTrace();
				
			}
    	   
    	}
    	
    	return SendData("http://lamp.cse.fau.edu/~oacosta/sharkinsert.php" + parameters);
    	
	}
	
	private String SendData(String serverURL)
	{
		String returnMessage = "";
		    	
		// This code allows the main thread to perform a network operation
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy); 
				
		// Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost(serverURL);
	    httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
	    
	    try {
	        
	    	// Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        
	        // Gets the response from the HTTP Post
	        returnMessage = response.getStatusLine().toString();
	        
	        // Get hold of the response entity
	        HttpEntity entity = response.getEntity();
	        
	        if (entity != null) {

	            // A Simple JSON Response Read
	            InputStream instream = entity.getContent();
	            returnMessage = convertStreamToString(instream);
	            // now you have the string representation of the HTML request
	            instream.close();
	        }
	        
	    } catch (ClientProtocolException e) {
	        // TODO Auto-generated catch block
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	    }
		
		return returnMessage;
	
	}
	
	private String convertStreamToString(InputStream is) {
	    
		// Converts the inputStream into a String
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();

	    String line = null;
	    try {
	        while ((line = reader.readLine()) != null) {
	            sb.append(line + "\n");
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            is.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	    return sb.toString();
	}
	
	public List<sharkPosition> GetBlackTip()
	{
		return GetSharkLocation("http://lamp.cse.fau.edu/~oacosta/blacktipsharks.php");
	}
	
	public List<sharkPosition> GetSpinner()
	{
		return GetSharkLocation("http://lamp.cse.fau.edu/~oacosta/spinnersharks.php");
	}
	
	private List<sharkPosition> GetSharkLocation(String pageURL)
	{
		String responseMessage = SendData(pageURL);
			
		List<sharkPosition> sharkPositionList = new ArrayList<sharkPosition>();
		
        JSONObject jsonResponse;
               
        try {
               
             // Converts the responseMessage to a jSon object 
             jsonResponse = new JSONObject(responseMessage);
               
             // Gets the jSon content for the "Android" jSon array
             JSONArray jsonMainNode = jsonResponse.optJSONArray("Android");
               
             // Iterates through each jSon node and adds it to the list 
             for(int i=0; i < jsonMainNode.length(); i++) 
             {
                 // Get Object for each JSON node
                 JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                 
                 // Creates a new SharkPosition
                 sharkPosition newPosition = new sharkPosition();
                 newPosition.sharkType = jsonChildNode.optString("sharkType").toString();
                 newPosition.latitude = jsonChildNode.optString("latitude").toString();
                 newPosition.longitude = jsonChildNode.optString("longitude").toString();
                 newPosition.title = jsonChildNode.optString("boatName");
                 newPosition.comment = jsonChildNode.optString("comment");
                 
                 // Adds the new shark position to the list 
                 sharkPositionList.add(newPosition);                 
                  
             }
              
         } catch (JSONException e) {
   
             e.printStackTrace();
         }
		
		// Returns the list of shark position
		return sharkPositionList;
	}
	
	
	
	// Is used by the GetSharkLocation method
	public class sharkPosition{
		public String sharkType;
		public String longitude;
		public String latitude;
		public String title;
		public String comment;
	}
	
}
